<?php
/**
 * @file
 * General HOOKs and API of "uuid_node_pane" module.
 */

/**
 * Implements hook_ctools_plugin_directory().
 */
function uuid_node_pane_ctools_plugin_directory($owner, $plugin_type) {
  if ($owner == 'ctools' && $plugin_type == 'content_types') {
    return 'plugins/' . $plugin_type;
  }
}

/**
 * Function to load the node by uuid.
 *
 * @param string $uuid
 *   A UUID number.
 *
 * @return int|FALSE
 *   A node nid or false.
 */
function uuid_node_pane_get_nid_by_uuid($uuid) {
  $uuids = drupal_static('node_nids_by_uuids', array());

  if (!isset($uuids[$uuid])) {
    $uuids[$uuid] = db_select('node', 'n')
      ->fields('n')
      ->condition('uuid', $uuid)
      ->execute()
      ->fetchField();

    if (!$uuids[$uuid]) {
      $uuids[$uuid] = FALSE;
    }
  }

  return $uuids[$uuid];
}

/**
 * Function to load the node by uuid.
 *
 * @param string $uuid
 *   A UUID number.
 *
 * @return object|FALSE
 *   A node object or false.
 */
function uuid_node_pane_get_none_by_uuid($uuid) {
  return ($nid = uuid_node_pane_get_nid_by_uuid($uuid)) ? node_load($nid) : FALSE;
}
